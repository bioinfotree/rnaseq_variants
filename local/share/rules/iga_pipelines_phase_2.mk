# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/$(PRJ)/accepted_hits_RG.bam as BAM_RG

filter_splitNtrim_realign_rna-seq_GATK_prep_pipeline.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/$@


.PHONY: launch
launch: filter_splitNtrim_realign_rna-seq_GATK_prep_pipeline.ptp
	!threads
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--label "$@.$(basename $<)" \
	--templates \
	input_bam="$$(readlink -f $(BAM_RG))" \
	output_folder="$$PWD" \
	prefix="$(PRJ)" \
	reference="$(REF)"

.PHONY: test
test:
	@echo $$(readlink -f $(BAM_RG))

ALL += filter_splitNtrim_realign_rna-seq_GATK_prep_pipeline.ptp \
	launch

INTERMEDIATE +=

CLEAN += 