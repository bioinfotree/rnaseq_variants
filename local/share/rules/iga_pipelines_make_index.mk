# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

REFERENCE ?= 

log:
	mkdir -p $@

tmp:
	mkdir -p $@

reference.fasta:
	zcat <$(REFERENCE) >$@

# index genome
reference.fasta.1.bt2: log reference.fasta
	!threads
	$(load_modules); \
	bowtie2-build \
	-f $^2 $^2 \
	2>&1 \
	| tee $</$(basename $@).log

reference.dict: log tmp reference.fasta
	!threads
	$(load_modules); \
	java -Xmx4g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar CreateSequenceDictionary \
	REFERENCE=$(REFERENCE) \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	3>&1 1>&2 2>&3 \
	| tee $</picard-CreateSequenceDictionary.$@.log

dummy.gtf: reference.fasta
	module load lang/python/2.7.3; \
	/iga/scripts/dscaglione/gff_gtf_manipulation/dummy_gtf.py \
	--fasta $< \
	--out-gtf $@


ALL += reference.fasta \
	reference.fasta.1.bt2 \
	reference.dict \
	dummy.gtf

INTERMEDIATE +=

CLEAN += log \
	tmp \
	$(wildcard reference.*.bt2)
