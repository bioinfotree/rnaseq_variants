# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>


default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/$@

R1.fastq.gz:
	cat $(R1) >$@

R2.fastq.gz:
	cat $(R2) >$@

.PHONY: launch
launch: default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp R1.fastq.gz R2.fastq.gz
	!threads
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--label "$@.$(basename $<)" \
	--templates \
	library_type=fr-unstranded \
	output_dir="$$PWD" \
	output_prefix="$(PRJ)" \
	reference_sequence="$(REF)" \
	rgid="$(RGID)" \
	rglb="$(RGLB)" \
	rgsm="$(PRJ)" \
	annotation=$(GTF) \
	read_1=$^2 \
	read_2=$^3 \
	threads=$$THREADNUM


ALL += default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp \
	R1.fastq.gz \
	R2.fastq.gz \
	launch

INTERMEDIATE +=

CLEAN += R1.fastq.gz \
	R2.fastq.gz