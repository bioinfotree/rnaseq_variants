# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

REFERENCE ?= 

log:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@

# index genome
Genome: log reference.fasta
	!threads
	$(load_modules); \
	STAR \
	--runMode genomeGenerate \
	--genomeDir $$PWD \
	--genomeFastaFiles $^2 \
	--limitGenomeGenerateRAM 43476084778 \
	--runThreadN $$THREADNUM \
	2>&1 \
	| tee $</$(basename $@).log


ALL += reference.fasta \
	Genome

INTERMEDIATE +=

CLEAN += log