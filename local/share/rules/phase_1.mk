# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>


SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 
REFERENCE ?= 

log:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@

# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
RG_FASTQ_GZ = $(addsuffix .RG,$(SAMPLES))
%.1.fastq.gz  %.2.fastq.gz %.RG:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),1) $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf $(call get,$(SAMPLE),2) $(SAMPLE).2.fastq.gz) \
		$(shell echo -E '$(call get,$(SAMPLE),RG)' >$(SAMPLE).RG) \
	) \
	&& sleep 3

# alignment jobs were executed as follows
Aligned.out.sam: log $(1_FASTQ_GZ) $(2_FASTQ_GZ)
	!threads
	$(load_modules); \
	STAR \
	--runMode alignReads \
	--readFilesCommand zcat \
	--genomeDir $(dir $(REFERENCE)) \
	--readFilesIn <(cat AR_brain.1.fastq.gz AR_norm_brain.1.fastq.gz) \
	--runThreadN $$THREADNUM \
	2>&1

# file containing splice junctions
SJ.out.tab: Aligned.out.sam
	touch $@

# for the 2-pass STAR, a new index is then created using splice junction information contained in the file SJ.out.tab from the first pass
Genome: log reference.fasta SJ.out.tab
	!threads
	$(load_modules); \
	STAR \
	--runMode genomeGenerate \
	--genomeDir $$PWD \
	--genomeFastaFiles $^2 \
	--sjdbFileChrStartEnd $^3 \
	--limitGenomeGenerateRAM 43476084778 \
	--sjdbOverhang 100 \
	--runThreadN $$THREADNUM \
	2>&1


ALL += log \
	reference.fasta \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	Aligned.out.sam \
	SJ.out.tab \
	Genome

INTERMEDIATE +=

CLEAN += $(wildcard *.txt) \
	$(wildcard *.out) \
	SA \
	SAindex \
	sjdbList.out.tab