# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_2/$(PRJ)_q10_FM_markdup_realign.bam as REALN_BAM

tmp:
	mkdir -p $@

logs:
	mkdir -p $@

GATK_haplotypecaller_optional_recalibration_RNAseq.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/$@

fake.vcf:
	head -n 67 <$(FAKE_VCF) >$@

.PHONY: launch
launch: GATK_haplotypecaller_optional_recalibration_RNAseq.ptp fake.vcf
	!threads
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--label "$@.$(basename $<)" \
	--skip-steps s1,s2,s3,s4,s5,s6,s7,s8,s10,s11 \   * skip SNP calling *
	--templates \
	input_bam="$(REALN_BAM)" \
	out_prefix="$(PRJ)" \
	gatk_formatted_snp_database_vcf="$$(readlink -f $^2)" \
	heterozygosity=0.0007 \
	output_folder="$$PWD" \
	output_mode="GVCF" \
	reference_fasta="$(REF)" \
	sample_name="$(PRJ)"

## INDEL left realignment: avoided since very few indel are realigned
# AR_brain_recal_realign.bam: tmp logs
	# java -Xmx2g \
	# -Djava.io.tmpdir=tmp \
	# -jar /iga/stratocluster/packages/sw/bio/gatk/3.3-0/GenomeAnalysisTK.jar \
	# -R $(REF) \
	# -T LeftAlignIndels \
	# -I AR_brain_recalibrated.bam \
	# -o $@ \
	# 2>&1 \
	# | tee logs/LeftAlignIndels.$@.log


.PHONY: test
test:
	@echo $$(readlink -f $(BAM_RG))


ALL += GATK_haplotypecaller_optional_recalibration_RNAseq.ptp \
	fake.vcf \
	launch

INTERMEDIATE +=

CLEAN += 