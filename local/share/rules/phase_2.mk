# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/Genome as REFERENCE2

SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 



# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
RG_FASTQ_GZ = $(addsuffix .RG,$(SAMPLES))
%.1.fastq.gz  %.2.fastq.gz %.RG:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),1) $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf $(call get,$(SAMPLE),2) $(SAMPLE).2.fastq.gz) \
		$(shell echo -E '$(call get,$(SAMPLE),RG)' >$(SAMPLE).RG) \
	) \
	&& sleep 3

all.fastq:
	cat $(1_FASTQ_GZ) \
	| zcat >$@
all.2.fastq:
	cat $(_FASTQ_GZ) \
	| zcat >$@

# SEP :=,
# JOIN = $(call merge,$(SEP),$1 $2)
# --readFilesIn $(call pairmap,JOIN,$(1_FASTQ_GZ),$(2_FASTQ_GZ)) \   * great pairmap function *
#  The resulting index is then used to produce the final alignments as follows:
Aligned.out.bam: $(1_FASTQ_GZ) $(2_FASTQ_GZ)
	!threads
	$(load_modules); \
	STAR \
	--runMode alignReads \
	--readFilesCommand zcat \
	--genomeDir $(dir $(REFERENCE2)) \
	--readFilesIn <(cat AR_brain.1.fastq.gz AR_norm_brain.1.fastq.gz) \
	--runThreadN $$THREADNUM \
	--outStd SAM \
	| samtools sort -l 9 -@ $$THREADNUM - \   * maximum compression *
	2>$@ 3>&1 1>&2 2>&3




ALL += $(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	Aligned.out.bam

INTERMEDIATE +=

CLEAN += $(wildcard *.txt) \
	$(wildcard *.out) \
	SJ.out.tab